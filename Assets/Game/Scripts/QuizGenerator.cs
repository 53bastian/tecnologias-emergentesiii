﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[System.Serializable]
public class Quiz
{
    public Sprite Question;
    public Sprite Answer1;
    public Sprite Answer2;
    public Sprite Answer3;
    public int CorrectAnswer;
}
public class QuizGenerator : MonoBehaviour
{
    public Transform[] Answers;
    
    public int NumOfCorrectAnswers;
    public int CurrentQuestion = 0;
    public GameObject CarIcon;
    public AudioClip LoseFx;
    public AudioClip WinFx;
    public AudioClip GameFinishFx;
    public AudioClip FinishedTimeFx;
    public Image Question;
    public Image Answer1;
    public Image Answer2;
    public Image Answer3;
    public Image CorrectAnswer1;
    public Image CorrectAnswer2;
    public Image CorrectAnswer3;
    public Image IncorrectAnswer1;
    public Image IncorrectAnswer2;
    public Image IncorrectAnswer3;

    public GameObject Point1;
    public GameObject Point2;
    public GameObject Point3;
    
    public int CurrentAnswer;
    public Quiz[] Quizzes;

    public bool AnsweredQuestion;

    public GameObject GameScreen;
    public GameObject FinishScreen;

    public GameObject ConfettiParticles;
    public GameObject RainParticles;

    public Image CorrectAnswersImage;
    public Sprite ZeroAnswerCorrectSprite;
    public Sprite OneAnswerCorrectSprite;
    public Sprite TwoAnswerCorrectSprite;
    public Sprite ThreeAnswerCorrectSprite;

    public Image Price;
    public Image ContinueButton;

    public Sprite NoPrice;
    public Sprite MediumPrice;
    public Sprite MaxPrice;
    public Sprite NoPriceButton;
    public Sprite MediumPriceButton;
    public Sprite MaxPriceButton;

    public List<int> IndexAnswers;
    
    // Start is called before the first frame update
    void Start()
    {
        LoadQuiz();
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentQuestion == 1)
        {
            CarIcon.transform.position = new Vector3(Point1.transform.position.x, CarIcon.transform.position.y, CarIcon.transform.position.z);
        } else if (CurrentQuestion == 2)
        {
            Vector3 newPos = new Vector3(Point2.transform.position.x, CarIcon.transform.position.y, CarIcon.transform.position.z);
            CarIcon.transform.position = Vector3.Lerp(CarIcon.transform.position, newPos, 0.2f);
        } else if (CurrentQuestion == 3)
        {
            Vector3 newPos = new Vector3(Point3.transform.position.x, CarIcon.transform.position.y, CarIcon.transform.position.z);
            CarIcon.transform.position = Vector3.Lerp(CarIcon.transform.position, newPos, 0.2f);
        }

        if (GetComponent<Counter>().TimeFinished)
        {
            ShowAnswers(true);
        }

        if (NumOfCorrectAnswers <= 1)
        {
            Price.sprite = NoPrice;
            ContinueButton.sprite = NoPriceButton;
        } else if (NumOfCorrectAnswers == 2)
        {
            Price.sprite = MediumPrice;
            ContinueButton.sprite = MediumPriceButton;
        }
        else
        {
            Price.sprite = MaxPrice;
            ContinueButton.sprite = MaxPriceButton;
        }
    }

    public void LoadQuiz()
    {
        CurrentQuestion++;

        if (CurrentQuestion >= 4)
        {   
            GameScreen.SetActive(false);
            FinishScreen.SetActive(true);
            ConfettiParticles.SetActive(true);
            ConfettiParticles.GetComponent<ParticleSystem>().Play();
            RainParticles.SetActive(true);
            RainParticles.GetComponent<ParticleSystem>().Play();

            if (NumOfCorrectAnswers == 0)
            {
                CorrectAnswersImage.sprite = ZeroAnswerCorrectSprite;
            } else if (NumOfCorrectAnswers == 1)
            {
                CorrectAnswersImage.sprite = OneAnswerCorrectSprite;
            } else if (NumOfCorrectAnswers == 2)
            {
                CorrectAnswersImage.sprite = TwoAnswerCorrectSprite;
            } else if (NumOfCorrectAnswers == 3)
            {
                CorrectAnswersImage.sprite = ThreeAnswerCorrectSprite;
            }
            
            SoundManager.PlaySfx(GameFinishFx);
            
            IndexAnswers.Clear();
            
            ResetValues();
        }
        
        int index = Random.Range(0, Quizzes.Length);
        bool nextAnswer = false;
        while (!nextAnswer)
        {
            if (IndexAnswers.Contains(index))
            {
                if(index == Quizzes.Length - 1)
                {
                    index = 0;
                }

                index++;
                
                print("NEW QUESTION");
            }
            else
            {
                IndexAnswers.Add(index);
                foreach (var indexes in IndexAnswers)
                {
                    print(indexes);
                }
                nextAnswer = true;
            }
        }
        
        Quiz currentQuiz = Quizzes[index];
        Question.sprite = currentQuiz.Question;
        Answer1.sprite = currentQuiz.Answer1;
        Answer2.sprite = currentQuiz.Answer2;
        Answer3.sprite = currentQuiz.Answer3;
        CurrentAnswer = currentQuiz.CorrectAnswer;
        AnsweredQuestion = false;
        
        CorrectAnswer1.gameObject.SetActive(false);
        CorrectAnswer2.gameObject.SetActive(false);
        CorrectAnswer3.gameObject.SetActive(false);
        
        IncorrectAnswer1.gameObject.SetActive(false);
        IncorrectAnswer2.gameObject.SetActive(false);
        IncorrectAnswer3.gameObject.SetActive(false);
        
        SwapObjects();
    }

    public void CompareAnswers(int IndexAnswer)
    {
        if (AnsweredQuestion)
        {
            return;
        }
        if (IndexAnswer == CurrentAnswer)
        {
            NumOfCorrectAnswers++;
            SoundManager.PlaySfx(WinFx);
        }
        else
        {
            SoundManager.PlaySfx(LoseFx);
        }
        ShowAnswers();
    }

    public void ShowAnswers(bool FromClock = false)
    {
        GetComponent<Counter>().StopCounter = true;
        if (AnsweredQuestion)
        {
            return;
        }
        if (CurrentAnswer == 1)
        {
            CorrectAnswer1.gameObject.SetActive(true);
            IncorrectAnswer2.gameObject.SetActive(true);
            IncorrectAnswer3.gameObject.SetActive(true);
        } else if (CurrentAnswer == 2)
        {
            IncorrectAnswer1.gameObject.SetActive(true);
            CorrectAnswer2.gameObject.SetActive(true);
            IncorrectAnswer3.gameObject.SetActive(true);
        } else if (CurrentAnswer == 3)
        {
            IncorrectAnswer1.gameObject.SetActive(true);
            IncorrectAnswer2.gameObject.SetActive(true);
            CorrectAnswer3.gameObject.SetActive(true);
        }

        if (!FromClock)
        {
            AnsweredQuestion = true;
        }
        else
        {
            SoundManager.PlaySfx(FinishedTimeFx);
            AnsweredQuestion = true;
        }
        
        StartCoroutine(SettingOffInSeconds());
    }

    IEnumerator SettingOffInSeconds()
    {
        GetComponent<Counter>().StopCounter = true;
        yield return new WaitForSeconds(2);
        LoadQuiz();
        GetComponent<Counter>().RestartCounter();
    }

    public void ResetValues()
    {
        NumOfCorrectAnswers = 0;
        CurrentQuestion = 1;
        
        AnsweredQuestion = false;
        
        CorrectAnswer1.gameObject.SetActive(false);
        CorrectAnswer2.gameObject.SetActive(false);
        CorrectAnswer3.gameObject.SetActive(false);
        
        IncorrectAnswer1.gameObject.SetActive(false);
        IncorrectAnswer2.gameObject.SetActive(false);
        IncorrectAnswer3.gameObject.SetActive(false);
    }

    public void SwapObjects()
    {
        for (int t = 0; t < Answers.Length; t++ )
        {
            Vector3 tmp = Answers[t].position;
            int r = Random.Range(t, Answers.Length);
            Answers[t].position = Answers[r].position;
            Answers[r].position = tmp;
        }
    }
}